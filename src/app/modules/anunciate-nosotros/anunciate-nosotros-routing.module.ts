import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AnunciateNosotrosComponent} from "./components/anunciate-nosotros/anunciate-nosotros.component";
import {
  AnunciateNosotrosGestionarComponent
} from "./components/anunciate-nosotros-gestionar/anunciate-nosotros-gestionar.component";
import {
  AnunciateNosotrosEditarComponent
} from "./components/anunciate-nosotros-editar/anunciate-nosotros-editar.component";

const routes: Routes = [
  {
    path: 'registrar',
    component: AnunciateNosotrosComponent
  },
  {
    path: 'gestionar',
    component: AnunciateNosotrosGestionarComponent
  },
  {
    path: 'editar/:idRegistro',
    component: AnunciateNosotrosEditarComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnunciateNosotrosRoutingModule { }
