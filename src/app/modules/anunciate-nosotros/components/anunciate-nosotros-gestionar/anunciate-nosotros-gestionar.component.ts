import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {
  EmethAlertsService,
  EmethColumn,
  EmethColumnsDef,
  EmethDialogsService
} from "@ceneval_plataforma_emeth/ngx-emeth";
import {Router} from "@angular/router";

@Component({
  selector: 'app-anunciate-nosotros-gestionar',
  templateUrl: './anunciate-nosotros-gestionar.component.html',
  styleUrls: ['./anunciate-nosotros-gestionar.component.scss']
})
export class AnunciateNosotrosGestionarComponent implements OnInit {
  dataSource: MatTableDataSource<Formulario> = new MatTableDataSource<Formulario>();
  columnsDef: EmethColumnsDef = new EmethColumnsDef();

  constructor(
    private alertsService: EmethAlertsService,
    private router: Router,
    private emethDialogsService: EmethDialogsService
  ) {
    this.columnsDef.displayedColumns = [
      'nombre',
      'nombreEmpresa',
      'email',
      'lada',
      'telefono',
      'categoriaNegocio',
      'entidadFederativa',
      'idiomasHablados',
      'acciones'
    ];
    this.columnsDef.columns.push(new EmethColumn('nombre', 'AN-CU2.nombre'));
    this.columnsDef.columns.push(new EmethColumn('nombreEmpresa', 'AN-CU2.nombre_empresa'));
    this.columnsDef.columns.push(new EmethColumn('email', 'AN-CU2.email'));
    this.columnsDef.columns.push(new EmethColumn('lada', 'AN-CU2.lada'));
    this.columnsDef.columns.push(new EmethColumn('telefono', 'AN-CU2.telefono'));
    this.columnsDef.columns.push(new EmethColumn('categoriaNegocio', 'AN-CU2.categoria'));
    this.columnsDef.columns.push(new EmethColumn('entidadFederativa', 'AN-CU2.entidad_federativa'));
    this.columnsDef.columns.push(new EmethColumn('idiomasHablados', 'AN-CU2.idiomas_hablados'));
  }

  ngOnInit(): void {
    this.cargarDatos();
  }

  regresar() {
    this.router.navigate(['home']);
  }

  editarFormulario(idFormulario: number) {
    console.log('Editar formulario', idFormulario);
  }

  eliminarRegistro(idFormulario: number) {
    this.emethDialogsService.openConfirmDialog({
      title: 'etiquetas.confirmacion_accion',
      body: 'AN-CU3.body'
    }).afterClosed().subscribe(result => {
      if (!result)
        return;
      this.alertsService.setSuccess(['AN-CU3.msg004']);
    });
  }

  cargarDatos(){
    this.dataSource.data = [
      {
        id: 1,
        nombre: 'David',
        nombreEmpresa: 'Empresa1',
        email: 'davidjchavez98@gmail.com',
        lada: '+52',
        telefono: '0000000000',
        categoriaNegocio: 'Categoria1',
        entidadFederativa: 'Entidad1',
        idiomasHablados: 'Idioma1, Idioma2'
      },
      {
        id: 2,
        nombre: 'David',
        nombreEmpresa: 'Empresa1',
        email: 'davidjchavez98@gmail.com',
        lada: '+52',
        telefono: '0000000000',
        categoriaNegocio: 'Categoria1',
        entidadFederativa: 'Entidad1',
        idiomasHablados: 'Idioma1, Idioma2'
      }
    ];
  }

}

type Formulario = {
  id: number;
  nombre: string;
  nombreEmpresa: string;
  email: string;
  lada: string;
  telefono: string;
  categoriaNegocio: string;
  entidadFederativa: string;
  idiomasHablados: string;
}
