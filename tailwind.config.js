/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        primary: "#003764",
        secondary: "#FF6C0E",
        gris: "#00000020"
      },
    },
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        'ceneval': {
          'primary': '#003764',           /* Primary color */
          'primary-focus': '#002348',     /* Primary color - focused */
          'primary-content': '#ffffff',   /* Foreground content color to use on primary color */

          'secondary': '#ff6c0e',         /* Secondary color */
          'secondary-focus': '#ff4f08',   /* Secondary color - focused */
          'secondary-content': '#ffffff', /* Foreground content color to use on secondary color */

          'accent': '#d62121',            /* Accent color */
          'accent-focus': '#c61414',      /* Accent color - focused */
          'accent-content': '#ffffff',    /* Foreground content color to use on accent color */

          'neutral': '#3d4451',           /* Neutral color */
          'neutral-focus': '#2a2e37',     /* Neutral color - focused */
          'neutral-content': '#ffffff',   /* Foreground content color to use on neutral color */

          'base-100': '#ffffff',          /* Base color of page, used for blank backgrounds */
          'base-200': '#f9fafb',          /* Base color, a little darker */
          'base-300': '#d1d5db',          /* Base color, even more darker */
          'base-content': '#1f2937',      /* Foreground content color to use on base color */

          'info': '#2094f3',              /* Info */
          'success': '#009485',           /* Success */
          'warning': '#ff9900',           /* Warning */
          'error': '#ff2424',             /* Error */
          'gris': '#00000020',              /* Gris */
        }
      }
    ]
  }
}
